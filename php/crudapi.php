<?php
class CrudApi {
    
   protected $mysqli;
    const LOCALHOST = '127.0.0.1';
    const USER = 'root';
    const PASSWORD = '';
    const DATABASE = 'webmap';
    
    /**
     * Constructor de clase
     */
    public function __construct() {           
        try{
            //conexión a base de datos
            $this->mysqli = new mysqli(self::LOCALHOST, self::USER, self::PASSWORD, self::DATABASE);
        }catch (mysqli_sql_exception $e){
            //Si no se puede realizar la conexión
            http_response_code(500);
            exit;
        }     
    } 
    
    /**
     * obtiene un solo registro dado su ID
     * @param int $id identificador unico de registro
     * @return Array array con los registros obtenidos de la base de datos
     */
    public function getStudents($id=0){      
        $stmt = $this->mysqli->prepare("SELECT * FROM alumnos WHERE id_alumnos= ?");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $result = $stmt->get_result();        
        $alumnos = $result->fetch_all(MYSQLI_ASSOC); 
        $stmt->close();
        return $alumnos;              
    }
    public function getStudentsAll(){      
        $stmt = $this->mysqli->prepare("SELECT * FROM alumnos");
        $stmt->execute();
        $result = $stmt->get_result(); 
        $alumnos = $result->fetch_all(MYSQLI_ASSOC); 
        $stmt->close();
        $stmt2 = $this->mysqli->prepare("SELECT * FROM horario");
        $stmt2->execute();
        $result2 = $stmt2->get_result();      
        $horario = $result2->fetch_all(MYSQLI_ASSOC); 
        $stmt2->close();
        $TODO = array('alumnos' => $alumnos, 'horario' => $horario);
        return $TODO;              
    }
}